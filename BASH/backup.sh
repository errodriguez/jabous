#!/usr/bin/env bash

#
# backup.sh v1.0
#
# A BASH script to backup data
#
#

function Usage(){
    echo "Usage: $CMD [-n] SOURCE|iDrive TARGET" 1>&2
    if [[ $1 == "full" ]]
       then echo ""
            echo "Examples:"
            echo "    ./$CMD iDrive Archive/test"
            echo ""
    fi 1>&2
}

function Sync(){
    echo -n "... "
    INCLUDE=""
    for i in $4
    do
        INCLUDE="--include $i/ "$INCLUDE
    done
    rsync --archive --update --delete \
        --modify-window=1 $DRY_RUN \
        $INCLUDE \
        --exclude "/*" \
        --exclude ".DS_Store" \
        --exclude ".localized" \
        "$2" "$3"
    echo "$1"
}
 
OPTSTRING="nh"
DRY_RUN=""
CMD=`basename $0`

while getopts $OPTSTRING OPT
do
  case $OPT in
      n ) DRY_RUN="--dry-run"
          ;;
      h ) Usage full
          exit 0
          ;;
      ? ) echo "Invalid option."
          exit 5
          ;;
      * ) Usage
          exit 1
          ;;
  esac
done

shift $((OPTIND - 1)) # remove options, keep non-option arguments

if [ $# -eq 0 ] 
   then Usage full
        exit 6
fi

if [[ $1 == "" ]]
   then Usage
        exit 2
fi

if [[ $2 == "" ]]
   then Usage
        exit 3
fi

TARGET=$2

if [[ ! -d $TARGET ]]
   then echo "$TARGET Target directory does NOT exists."
        exit 4
fi


if [[ $1 == "iDrive" ]]
   then 
        echo "$(date) starting iCloud drive backup."

        ICLOUD="$HOME/Library/Mobile Documents"

        SOURCE="$ICLOUD/iCloud~AsheKube~app~a-Shell/" 
        Sync "a-Shell" "$SOURCE" "$TARGET/a-Shell" "Documents"

        SOURCE="$ICLOUD/com~apple~Automator/"
        Sync "Automator" "$SOURCE" "$TARGET/Automator" "Documents"

        SOURCE="$ICLOUD/iCloud~AsheKube~Carnets/"
        Sync "Carnets" "$SOURCE" "$TARGET/Carnets" "Documents"

        SOURCE="$ICLOUD/com~apple~CloudDocs/"
        Sync "Downloads & Music" "$SOURCE" "$TARGET" "Downloads Music"

        echo -n "... "
        rsync --archive --update --delete \
            --modify-window=1 $DRY_RUN \
            --include "Drop Box/" \
            --exclude "/*" \
            --exclude ".DS_Store" \
            --exclude ".localized" \
            "$SOURCE" ${TARGET}
        echo "Drop Box"

        echo -n "... "
        SOURCE="$HOME/"
        rsync --archive --update --delete --modify-window=1 $DRY_RUN \
            --include "Desktop/" \
            --include "Documents/" \
            --exclude "/*" \
            --exclude ".DS_Store" \
            --exclude ".localized" \
            "$SOURCE" ${TARGET}
        echo "Desktop & Documents"

        SOURCE="$ICLOUD/com~apple~Keynote/Documents/"
        Sync "Keynote" "$SOURCE" "$TARGET/Keynote" "Documents"

        SOURCE="$ICLOUD/com~apple~Numbers/Documents/" 
        Sync "Numbers" "$SOURCE" "$TARGET/Numbers" "Documents"

        echo -n "... "
        SOURCE="$ICLOUD/iCloud~md~obsidian/Documents/" 
        rsync --archive --update --delete \
            --modify-window=1 $DRY_RUN \
            --include "Cloud vault/" \
            --exclude "/*" \
            --exclude ".DS_Store" \
            --exclude ".localized" \
            "$SOURCE" $TARGET/Obsidian
        echo "Obsidian"

        SOURCE="$ICLOUD/com~apple~Pages/Documents/"
        Sync "Pages" "$SOURCE" "$TARGET/Pages" "Documents"

        SOURCE="$ICLOUD/com~apple~Preview/Documents/"
        Sync "Preview" "$SOURCE" "$TARGET/Preview" "Documents"

        SOURCE="$ICLOUD/com~apple~ScriptEditor2/Documents/" 
        Sync "Script Editor" "$SOURCE" "$TARGET/Script Editor" "Documents"

        SOURCE="$ICLOUD/iCloud~is~workflow~my~workflows/Documents/" 
        Sync "Shortcuts" "$SOURCE" "$TARGET/Shortcuts" "Documents"

        SOURCE="$ICLOUD/com~apple~TextEdit/Documents/"
        Sync "TextEdit" "$SOURCE" "$TARGET/TextEdit" "Documents"

        echo "$(date) ending iCloud drive backup."
   else
        echo "$(date) starting $HOME backup."
        TARGET=/Volumes/Archive/Codex

        rsync -v --archive --update --delete --include "*" --exclude "/*" --exclude ".DS_Store" --exclude ".localized" $HOME/Data $TARGET$HOME/
        rsync -v --archive --update --delete --include "*" --exclude "/*" --exclude ".DS_Store" --exclude ".localized" $HOME/Laboratory $TARGET$HOME/
        rsync -v --archive --update --delete --include "*" --exclude "/*" --exclude ".DS_Store" --exclude ".localized" $HOME/Music $TARGET$HOME/
        rsync -v --archive --update --delete --include "*" --exclude "/*" --exclude ".DS_Store" --exclude ".localized" $HOME/Pictures $TARGET$HOME/
        rsync -v --archive --update --delete --include "*" --exclude "/*" --exclude ".DS_Store" --exclude ".localized" $HOME/Projects $TARGET$HOME/

        echo "$(date) ending $HOME backup."
fi 
